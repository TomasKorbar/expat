#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/expat/Sanity/smoke
#   Description: Basic functionality test using xmlwf
#   Author: Petr Splichal <psplicha@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2009 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="expat"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"

        # create test xml documents
        rlRun "echo '<?xml version=\"1.0\"?>' > empty.xml" 0 "Creating empty.xml"
        rlRun "echo '<tag> <one><two></one></two> </tag>' > nesting.xml" \
                0 "Creating nesting.xml"
        rlRun "echo '<tag> <quote without=quote>is bad</quote> </tag>' > quoting.xml" \
                0 "Creating quoting.xml"
        rlRun "echo '<one>content</one><two>content</two>' > roots.xml" \
                0 "Creating roots.xml"
        rlRun "echo '<!DOCTYPE tag [ <!ENTITY ent SYSTEM \"nesting.xml\"> ]> <tag>&ent;</tag>' > external.xml" \
                0 "Creating empty.xml"
        rlRun "echo '<tag>Content with <nothing/> something <dive level=\"3\">nested</dive>.</tag>' > well.xml" \
                0 "Creating well.xml"
        rlRun "echo '<tag>ěščřžýáíé, 測試, בדיקה, परीक्षण, テスト, 테스트, тестирование, ทดสอบ</tag>'> utf8.xml" \
                0 "Creating utf8.xml"
    rlPhaseEnd

    rlPhaseStartTest
        # emtpy
        rlRun "xmlwf empty.xml | tee output" 0 "Testing empty xml"
        rlAssertGrep "no element found" "output"

        # proper nesting
        rlRun "xmlwf nesting.xml | tee output" 0 "Testing proper nesting"
        rlAssertGrep "mismatched tag" "output"

        # quoting
        rlRun "xmlwf < quoting.xml | tee output" 0 "Testing quoting (and redirection)"
        rlAssertGrep "invalid token" "output"

        # two roots
        rlRun "cat roots.xml | xmlwf | tee output" 0 "Testing proper nesting (and pipe)"
        rlAssertGrep "junk after document element" "output"

        # external
        rlRun "xmlwf -p external.xml | tee output" 0 "Testing external entity"
        rlAssertGrep "mismatched tag" "output"
        rlAssertGrep "error in processing external entity" "output"

        # well formed
        rlRun "xmlwf well.xml | tee output" 0 "Testing well formed document"
        rlAssertNotDiffer "output" "/dev/null"

        # utf8 characters
        rlRun "xmlwf utf8.xml | tee output" 0 "Testing document with utf8 characters"
        rlAssertNotDiffer "output" "/dev/null"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
